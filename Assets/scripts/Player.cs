﻿using UnityEngine;
using System.Collections;

public class Player
{
    public string name;
    public int score;

    public Player (string newName, int newScore)
    {
        name = newName;
        score = newScore;
    }

}
