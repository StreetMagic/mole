﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Control : MonoBehaviour
{

    [SerializeField]
    CapsuleCollider hamsterCollider;

    [SerializeField]
    Movement hamster;

    [SerializeField]
    Score scoreUI;

    [SerializeField]
    ParticleSystem boomEffect;

    [SerializeField]
    AudioSource simpleClick;

    [SerializeField]
    AudioSource positiveSound;

    [SerializeField]
    AudioSource negativeSound;

    [SerializeField]
    GameObject PlusLife;

    [SerializeField]
    GameObject MinusLife;

    GameObject PlusLifeClone;
    GameObject MinusLifeClone;
    int luck;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnMouseDown()
    {
        boomEffect.Play();
        hamsterCollider.enabled = false;
        hamster.currentAction = Movement.Actions.moveBot;
        hamster.MoveDownSpeed();
        scoreUI.ResultScore();
        if (hamster.goodSphere.gameObject.activeInHierarchy)
        {
            scoreUI.IncreaseLives();
            hamster.goodSphere.gameObject.SetActive(false);
            positiveSound.Play();
            getPlusLife();

        }
        else if (hamster.badSphere.gameObject.activeInHierarchy)
        {
            scoreUI.DecreaseLives();
            hamster.badSphere.gameObject.SetActive(false);
            negativeSound.Play();
            getMinusLife();
        }
        else
        {
            simpleClick.Play();
        }
    }

    public void getPlusLife()
    {
        PlusLifeClone = Instantiate(PlusLife, new Vector3(transform.position.x, transform.position.y+2f, transform.position.z), Quaternion.identity) as GameObject;
        Destroy(PlusLifeClone, 1.5f);
    }

    public void getMinusLife()
    {
        MinusLifeClone = Instantiate(MinusLife, new Vector3(transform.position.x, transform.position.y + 2f, transform.position.z), Quaternion.identity) as GameObject;
        Destroy(MinusLifeClone, 1.5f);
    }
}