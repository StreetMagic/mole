﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{

    [SerializeField]
    Transform mainMenu;

    [SerializeField]
    Transform saveScoreMenu;

    [SerializeField]
    Transform backgroundFade;

    [SerializeField]
    Times endGame;

    [SerializeField]
    Startparameters restart;

    [SerializeField]
    Text startPauseButton;

    [SerializeField]
    Leaderboard enterLeaderboard;

    bool gameNotStarted = true;

    // Use this for initialization
    void Start()
    {
        Time.timeScale = 0;
        mainMenu.gameObject.SetActive(true);
        backgroundFade.gameObject.SetActive(true);
        startPauseButton.text = "New Game";

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && !(endGame.currentGameEvent == Times.GameEvent.gameOver) && !gameNotStarted)
        {
            startPauseButton.text = "Resume";
            GameMenuPause();
        }
    }

    public void GameMenuPause()
    {
        if (backgroundFade.gameObject.activeInHierarchy == false)
        {
            Time.timeScale = 0;
            mainMenu.gameObject.SetActive(true);
            backgroundFade.gameObject.SetActive(true);
        }
        else if (mainMenu.gameObject.activeInHierarchy == true)
        {
            Time.timeScale = 1;
            mainMenu.gameObject.SetActive(false);
            backgroundFade.gameObject.SetActive(false);
        }
    }

    public void QuitButton()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit ();
#endif
    }

    public void BackButton()
    {
        if (endGame.currentGameEvent == Times.GameEvent.gameOver || gameNotStarted)
        {
            startPauseButton.text = "New Game";
            mainMenu.gameObject.SetActive(true);
        }
        else
        {
            mainMenu.gameObject.SetActive(true);
            startPauseButton.text = "Resume";
        }
    }

    public void StartPauseButton()
    {
        if (gameNotStarted)
        {
            Time.timeScale = 1;
            mainMenu.gameObject.SetActive(false);
            backgroundFade.gameObject.SetActive(false);
            gameNotStarted = false;
        }
        else if (endGame.currentGameEvent == Times.GameEvent.gameOver)
        {
            restart.ResetParameters();
            mainMenu.gameObject.SetActive(false);
            backgroundFade.gameObject.SetActive(false);
        }
        else GameMenuPause();
    }
    public void ShowGameoverPanel()
    {
        if (enterLeaderboard.getInLeaderboard)
        {
            saveScoreMenu.gameObject.SetActive(true);
        }
        else
        {
            mainMenu.gameObject.SetActive(true);
        }
        backgroundFade.gameObject.SetActive(true);
        startPauseButton.text = "New Game";
    }
}
