﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Times : MonoBehaviour
{
    [SerializeField]
    List<Movement> hamsters;

    [SerializeField]
    Score round;

    [SerializeField]
    Slider timeBar;

    [SerializeField]
    Text timeBarText;

    [SerializeField]
    float roundTime;

    [SerializeField]
    AudioSource endRoundSound;

    public enum GameEvent {newRound, showFinishText, showStartText, gameOver}
    public GameEvent currentGameEvent;
    float realTime;
    float showTime;

    // Use this for initialization
    void Start()
    {
        currentGameEvent = GameEvent.newRound;
        timeBar.maxValue = roundTime;
        
    }

    // Update is called once per frame
    void Update()
    {

        if (currentGameEvent == GameEvent.newRound)
        {
            timeBar.value = realTime;
            timeBarText.text = "" + System.Math.Round(realTime,1);
            realTime += Time.deltaTime;
            if (realTime >= roundTime)
            {
                endRoundSound.Play();
                DiactivateHamster();
                currentGameEvent = GameEvent.showFinishText;
                round.roundCounter.enabled = true;
            }
        }
        else if (currentGameEvent == GameEvent.showFinishText)
        {
            round.ShowEndText();
        }
        if (currentGameEvent == GameEvent.showFinishText && Input.GetKeyDown(KeyCode.Space))
        {
            round.ShowRoundNumber();
            currentGameEvent = GameEvent.showStartText;
        }
        else if (currentGameEvent == GameEvent.showStartText)
        {
            round.ShowStartText();
            showTime += Time.deltaTime;
            if (showTime >= 1.1f)
            {
                round.roundCounter.enabled = false;
                for (int i = 0; i < hamsters.Count; i++)
                {
                    hamsters[i].endRound = false;
                    hamsters[i].SpeedIncrease();
                    hamsters[i].hamsterCollider.enabled = true;
                    hamsters[i].currentAction = Movement.Actions.waitBot;
                }
                showTime = 0;
                realTime = 0;
                currentGameEvent = GameEvent.newRound;

            }
        }
        if (currentGameEvent == GameEvent.gameOver)
        {
            DiactivateHamster();
        }
    }

        public void DiactivateHamster()
    {
        for (int i = 0; i < hamsters.Count; i++)
        {
            hamsters[i].hamsterCollider.enabled = false;
            hamsters[i].currentAction = Movement.Actions.moveBot;
            hamsters[i].endRound = true;
        }
    }

    public void resetTimer()
    {
        realTime = 0;
        currentGameEvent = GameEvent.newRound;
    }
}
