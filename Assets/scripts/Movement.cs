﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField]
    public Transform goodSphere;

    [SerializeField]
    public Transform badSphere;

    [SerializeField]
    Transform topBorder;

    [SerializeField]
    Transform botBorder;

    [SerializeField]
    float speed;

    [SerializeField]
    float moveDownSpeed;

    [SerializeField]
    float waitTopMin;

    [SerializeField]
    float waitTopMax;

    [SerializeField]
    float waitBotMin;

    [SerializeField]
    float waitBotMax;

    [SerializeField]
    public CapsuleCollider hamsterCollider;

    [SerializeField]
    Score lives;

    public enum Actions { waitBot, moveTop, waitTop, moveBot }
    public Actions currentAction;
    float waitTopTime;
    float waitBotTime;
    float currentTime = 0;
    public bool endRound;
    float baseSpeed;
    float currentSpeed;
    bool UncommonHamster = false;
    bool uncommonHamsterCheck = false;
    bool StartMovementParametarsChecked = false;

    // actions  order : "waitBot" => "moveTop" => "waitTop" => "moveBot" ...


    // Use this for initialization
    void Start()
    {
        waitTopTime = Random.Range(waitTopMin, waitTopMax);
        waitBotTime = Random.Range(waitBotMin, waitBotMax);
        currentAction = Actions.waitBot;
        baseSpeed = speed;
        currentSpeed = speed;
    }

    // Update is called once per frame
    void Update()
    {
        if (currentAction == Actions.waitBot)
        {
            if (!StartMovementParametarsChecked)
            {
                StartMovementparametars();
            }
            if (currentTime <= waitBotTime)
            {
                currentTime += Time.deltaTime;
            }
            else
            {
                currentTime = 0;
                currentAction = Actions.moveTop;
            }
        }
        else if (currentAction == Actions.moveTop)
        {
            if (!uncommonHamsterCheck)
            {
                UncommonHamsterCheck();
                uncommonHamsterCheck = true;
            }
            Vector3 direction = topBorder.position - transform.position;
            transform.position += direction.normalized * speed * Time.deltaTime; ;
            if (direction.magnitude <= 0.1f)
            {
                currentAction = Actions.waitTop;
            }
        }
        else if (currentAction == Actions.waitTop)
        {
            if (currentTime <= waitTopTime)
            {
                currentTime += Time.deltaTime;
            }
            else
            {
                currentTime = 0;
                currentAction = Actions.moveBot;
            }
        }
        else if (currentAction == Actions.moveBot)
        {
            Vector3 direction = botBorder.position - transform.position;
            transform.position += direction.normalized * speed * Time.deltaTime;
            if (direction.magnitude <= 0.1f)
            {
                if (hamsterCollider.enabled == true && badSphere.gameObject.activeInHierarchy == false)
                {
                    lives.MinusLive();
                }
                if (!endRound)
                {
                    currentAction = Actions.waitBot;
                    StartMovementParametarsChecked = false;
                }
            }
        }
    }
    public void SpeedIncrease()
    {
        speed = speed + 0.2f * baseSpeed;
    }

    public void MoveDownSpeed()
    {
        currentSpeed = speed;
        speed = Mathf.Max(speed, moveDownSpeed);
    }

    public void ResetMovement()
    {
        waitTopTime = Random.Range(waitTopMin, waitTopMax);
        waitBotTime = Random.Range(waitBotMin, waitBotMax);
        currentAction = Actions.waitBot;
        speed = baseSpeed;
        currentTime = 0;
        endRound = false;
        UncommonHamster = false;
        uncommonHamsterCheck = false;
        StartMovementParametarsChecked = false;
    }

    private void UncommonHamsterCheck()
    {
        int luck = Random.Range(0, 10);
        if (luck == 5)
        {
            UncommonHamster = true;
            goodSphere.gameObject.SetActive(true);
        }
        else if (luck == 6)
        {
            UncommonHamster = true;
            badSphere.gameObject.SetActive(true);
        }
    }
    private void StartMovementparametars()
    {
        speed = currentSpeed;
        hamsterCollider.enabled = true;
        UncommonHamster = false;
        uncommonHamsterCheck = false;
        goodSphere.gameObject.SetActive(false);
        badSphere.gameObject.SetActive(false);
        StartMovementParametarsChecked = true;
    }
}
