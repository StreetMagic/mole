﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Leaderboard : MonoBehaviour
{


    [SerializeField]
    Text inputName;

    [SerializeField]
    List<Text> playerNames;

    [SerializeField]
    List<Text> playerScores;

    [SerializeField]
    Score finalScore;

    [SerializeField]
    InputField nameSpace;

    int leaderboardSize;
    bool scoreAccepted = false;
    public bool getInLeaderboard = false;

    List<Player> players = new List<Player>();

    public Player newPlayer = new Player("", -1);

    // Use this for initialization
    void Start()
    {
        LoadScores();
        FillLeaderboard();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SaveScores()
    {
        PlayerPrefs.SetInt("Leaderboard.count", players.Count);
        for (int i = 0; i < players.Count; ++i)
        {
            PlayerPrefs.SetString("Leaderboard [" + i + "].name", players[i].name);
            PlayerPrefs.SetInt("Leaderboard [" + i + "].score", players[i].score);
        }
    }

    private void LoadScores()
    {
        players.Clear();
        leaderboardSize = PlayerPrefs.GetInt("Leaderboard.count", 0);

        for (int i = 0; i < leaderboardSize; ++i)
        {
            Player currentplayer = new Player("", 0);
            currentplayer.name = PlayerPrefs.GetString("Leaderboard [" + i + "].name", "");
            currentplayer.score = PlayerPrefs.GetInt("Leaderboard [" + i + "].score", 0);
            players.Add(currentplayer);
        }
    }

    public void GetPlayerName()
    {
        if (newPlayer.score == -1)
        {
            newPlayer.name = inputName.text;
            newPlayer.score = finalScore.score;
        }
    }

    public void LeaderboardPlacement()
    {
        int currentPlayerCount = players.Count;
        if (players.Count == 0)
        {
            players.Add(newPlayer);
        }
        else
        {
            for (int i = 0; i < currentPlayerCount; i++)
                if (newPlayer.score >= players[i].score)
                {
                    players.Insert(i, newPlayer);
                    i = currentPlayerCount;
                    scoreAccepted = true;
                }
            if (currentPlayerCount < 5 && !scoreAccepted)
            {
                players.Add(newPlayer);
                scoreAccepted = true;
            }
        }
        while (players.Count > 5)
        {
            players.RemoveAt(5);
        }
    }

    public void FillLeaderboard()
    {
        for (int i = 0; i < players.Count; i++)
        {
            playerNames[i].text = players[i].name;
            playerScores[i].text = players[i].score.ToString();
        }
    }

    public void LeaderboardPlacementCheck()
    {
        for (int i = 0; i < players.Count; i++)
            if (finalScore.score >= players[i].score)
            {
                getInLeaderboard = true;
                i = players.Count;
            }
        if (players.Count == 0)
        {
            getInLeaderboard = true;
        }
    }
    public void resetButton()
    {
        for (int i = 0; i < players.Count; i++)
        {
            playerNames[i].text = "";
            playerScores[i].text = "";
        }
        players.Clear();
        PlayerPrefs.DeleteAll();
    }

    public void resetLeaderboard()
    {
        LoadScores();
        FillLeaderboard();
        scoreAccepted = false;
        newPlayer.score = -1;
        nameSpace.text = "";
        getInLeaderboard = false;
    }
}
