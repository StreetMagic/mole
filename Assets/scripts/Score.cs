﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{

    [SerializeField]
    Text scoreText;

    [SerializeField]
    Text roundText;

    [SerializeField]
    Text livesText;

    [SerializeField]
    Menu endgameMenu;

    [SerializeField]
    public Text roundCounter;

    [SerializeField]
    int lives;

    [SerializeField]
    Times endGame;

    [SerializeField]
    Leaderboard GetInLeaderboard;

    [SerializeField]
    AudioSource gameOverSound;

    int startLives;
    public int score = 0;
    public int roundNumber = 1;
    bool gameFinished = false;
    // Use this for initialization
    void Start()
    {
        roundText.text = "Round: " + roundNumber;
        livesText.text = "Lives: " + lives;
        scoreText.text = "Score: " + score;
        startLives = lives;
    }

    // Update is called once per frame
    void Update()
    {
        if (lives <= 0)
        {
            GameOver();          
        }
    }

    public void IncreaseLives()
    {
        lives++;
        livesText.text = "Lives: " + lives;
    }

    public void DecreaseLives()
    {
        lives--;
        livesText.text = "Lives: " + lives;
    }

    public void ResultScore()
    {
        score++;
        scoreText.text = "Score: " + score;
    }
    public void ShowRoundNumber()
    {
        roundNumber++;
        roundText.text = "Round: " + roundNumber;
    }
    public void ShowEndText()
    {
        roundCounter.text = "Round number " + roundNumber + " finished!" + "\n press 'Space' to continue";
    }
    public void ShowStartText()
    {
        roundCounter.text = "Round number " + roundNumber + " begins!";
    }
    private void GameOver()
    {
        if (!gameFinished)
        {
            endGame.DiactivateHamster();
            endGame.currentGameEvent = Times.GameEvent.gameOver;
            GetInLeaderboard.LeaderboardPlacementCheck();
            endgameMenu.ShowGameoverPanel();
            gameFinished = true;
            gameOverSound.Play();
        }
    }
    public void MinusLive()
    {
        lives--;
        livesText.text = "Lives: " + lives;
    }

    public void ResetStats()
    {
        lives = startLives;
        score = 0;
        roundNumber = 1;
        gameFinished = false;
        roundText.text = "Round: " + roundNumber;
        livesText.text = "Lives: " + lives;
        scoreText.text = "Score: " + score;
    }
}