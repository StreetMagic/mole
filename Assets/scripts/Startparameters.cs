﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Startparameters : MonoBehaviour
{
    [SerializeField]
    List<Movement> hamsters;

    [SerializeField]
    Times timer;

    [SerializeField]
    Score resetStats;

    [SerializeField]
    Leaderboard resetBoard;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ResetParameters()
    {
        resetStats.ResetStats();
        for (int i = 0; i < hamsters.Count; i++)
        {
            hamsters[i].ResetMovement();
        }
        resetBoard.resetLeaderboard();
        timer.resetTimer();
        Time.timeScale = 1;
    }
}
